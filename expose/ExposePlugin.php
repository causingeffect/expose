<?php
namespace Craft;

/**
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2016 Causing Effect
 * @link		http://www.causingeffect.com
 *
 * Class ExposePlugin
 * @package Craft
 */
class ExposePlugin extends BasePlugin
{

	public function getName()
	{
		return Craft::t('Expose');
	}

    public function getDescription()
    {
        return Craft::t('Exposes plugin services to the yiic command line.');
    }

	public function getVersion()
	{
		return '0.1';
	}

	public function getDeveloper()
	{
		return 'Aaron Waldon';
	}

	public function getDeveloperUrl()
	{
		return 'http://causingeffect.com/';
	}

	public function hasCpSection()
	{
		return false;
	}

}