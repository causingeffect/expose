<?php
namespace Craft;

/**
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2016 Causing Effect
 * @link		http://www.causingeffect.com
 *
 * Class ExposeCommand
 * @package Craft
 */
class ExposeCommand extends BaseCommand
{
    /**
     * Exposes plugin services to the yiic command line.
     *
     * @param $service
     * @param $method
     * @param array $args
     * @return mixed
     *
     */
    public function actionRun($service, $method, $args=[])
    {
        //---------- get the services ----------
        $services = $this->getServicesArray();

        //---------- get a valid service ----------
        while (!isset($services[$service])) {
            echo 'The services "' . $service . '" could not be found. ';

            //offer a suggestion
            $closest = craft()->expose_helper->findClosestWordMatch($service, array_keys($services));
            if ( $closest ) {
                echo ' Did you mean "'.$closest.'"? ';
            }

            echo PHP_EOL;

            $response = $this->prompt('Please enter the service name:');
            if ($response) //we got a service name
            {
                $service = $response;
            }
            else //see if the user wants to bail
            {
                if ( $this->confirm('No name was entered. Do you want to quit?', true) )
                {
                    echo PHP_EOL . "Goodbye!";
                    return 1;
                }
            }
        }

        $classObject = craft()->{$service};

        //---------- get a valid method ----------
        //get all methods
        $all_methods = get_class_methods($classObject);
        //will hold an array of callable methods
        $methods = [];
        foreach ($all_methods as $all_method)
        {
            if (is_callable([$classObject,$all_method]))
            {
                $methods[] = $all_method;
            }
        }
        while (!in_array($method, $methods)) {
            echo 'The method "' . $method . '" is not callable. ';

            //offer a suggestion
            $closest = craft()->expose_helper->findClosestWordMatch($method, $methods);
            if ( $closest ) {
                echo ' Did you mean "'.$closest.'"? ';
            }

            echo PHP_EOL;

            $response = $this->prompt('Please enter the method name:');
            if ($response) //we got a method name
            {
                $method = $response;
            }
            else //see if the user wants to bail
            {
                if ( $this->confirm('No name was entered. Do you want to quit?', true) )
                {
                    echo PHP_EOL . "Goodbye!";
                    return 1;
                }
            }
        }

        //todo: make sure the argument count is correct before attempting to call the method

        //---------- call the method ----------
        echo 'Calling: "craft()->'.$service.'->'.$method.'"'.PHP_EOL;

        return call_user_func_array([$classObject, $method],$args);
    }

    /**
     * Shows a list of the available services.
     */
    public function actionListServices()
    {
        $services = $this->getServicesArray();
        echo 'The following services were found: ';
        echo PHP_EOL;
        echo implode(PHP_EOL, array_keys($services) );
        echo PHP_EOL;
    }

    /**
     * @return string
     */
    public function getHelp() {
        return '
Usage: yiic expose <action>
Actions:
    run --service=value --method=value arg1 arg2 etc
    listServices
';
    }

    /**
     * Returns a list of the available services.
     *
     * @return array
     */
    private function getServicesArray()
    {
        $services = array();
        $components = craft()->getComponents(false);
        foreach ($components as $componentsName => $info) {
            if (is_array($info) && isset($info['class']))
            {
                $services[$componentsName] = $info['class'];
            }
        }
        ksort($services);

        return $services;
    }
}