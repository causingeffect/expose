<?php
namespace Craft;

/**
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2016 Causing Effect
 * @link		http://www.causingeffect.com
 *
 * Class Expose_HelperService
 * @package Craft
 */
class Expose_HelperService extends BaseApplicationComponent
{
    public function findClosestWordMatch($misspelled, $words) 
    {
        $closest = false;

        //no shortest distance found, yet
        $shortest = -1;

        //loop through words to find the closest
        foreach ($words as $word) {

            //calculate the distance between the input word, and the current word
            $lev = levenshtein($misspelled, $word);

            //if this distance is less than the next found shortest distance, OR if a next shortest word has not yet been found
            if ($lev <= $shortest || $shortest < 0) {
                //set the closest match, and shortest distance
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest;
    }
}