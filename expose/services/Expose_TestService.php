<?php
namespace Craft;

/**
 * @author		Aaron Waldon
 * @copyright	Copyright (c) 2016 Causing Effect
 * @link		http://www.causingeffect.com
 *
 * Class Expose_TestService
 * @package Craft
 */
class Expose_TestService extends BaseApplicationComponent
{
    public function test($name, $phoneType) {
        echo $name.' uses the '.$phoneType.' type of phone!';
    }
}