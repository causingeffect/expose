#![Expose Plugin](./expose/resources/expose.svg)

A Craft CMS plugin that exposes plugin services to the yiic command line.


##Installation

* Drop the `expose` folder into the `craft/plugins` directory. 
* Enable the plugin in the control panel. 
* Add the `yiic` file to your site's root folder. You may need to adjust the `$yiicPath` variable inside the file if you have a non-standard site directory structure.


##Usage

The expose console commands are listed below. Keep in mind that you can view their usage info from the command line by running `php yiic help expose`.

###The `run` Command

Runs Craft plugin services via the command line. The plugin allows any service's callable methods to be called.

Usage: `php yiic expose run --service=value --method=value arg1? arg2?`

* The `--service=` option is the name of the service.
* The `--method=` option is the name of the service's method to be called.
* The optional method arguments can be passed in after the required options.

####Example

There is an example method made available:

`php yiic expose run --service=expose_test --method=test Aaron Android`

Which should return:

`Aaron uses the Android type of phone!`


###The `listServices` Command

Returns a list of the available services.

Usage: `php yiic expose listServices`


##Thanks

This plugin was commissioned and initially funded by [Michael Rog](https://michaelrog.com/). Thanks Michael! PS: If you go to his site, type in the Konami Code.